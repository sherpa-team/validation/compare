Compare(1)                                                          Compare(1)



NNAAMMEE
       Compare - Program to plot histograms

SSYYNNOOPPSSIISS
       Compare [[--AA _c_o_n_f_i_g_f_i_l_e]] [[--pp _s_e_t_u_p_p_a_t_h]] [[--ff _s_e_t_u_p_f_i_l_e]]
               [[--DD _d_a_t_a_p_a_t_h]] [[--ii _f_i_l_e_b_e_g_i_n]] [[--II _f_i_l_e_e_n_d]]
               [[--TT _t_a_g_s]] [[--PP _o_u_t_p_a_t_h]] [[--FF _r_o_o_t_f_i_l_e]]
               [[--BB _t_e_x_f_i_l_e]] [[--HH _t_e_x_h_e_a_d_e_r]] [[--CC _t_e_x_t_a_i_l]]
               [[--WW _h_t_m_l_f_i_l_e]] [[--SS _h_t_m_l_h_e_a_d_e_r]] [[--EE _h_t_m_l_t_a_i_l]]
               [[--ww _h_t_m_l_t_i_t_l_e]] [[--tt _f_i_g_u_r_e_t_y_p_e]] [[--OO _o_u_t_p_u_t_l_e_v_e_l]]
               [[--aaookkbbnneehh]]

DDEESSCCRRIIPPTTIIOONN
       Compare  is an extension to the ROOT framework. It is dedicated to plot
       large amounts of data as produced for example when analysing the output
       of Monte Carlo event generators. In this context Compare can be invoked
       to plot histograms of particle spectra, eventually including data, gen-
       erate  difference  plots,  rebin histograms, carry out algebraic opera-
       tions on the histograms, integrate  or  differentiate  them  and  write
       newly created histograms to disk. Once a full setup, specifying what to
       do with the input data is generated, input paths and files can be  eas-
       ily changed to carry out the same procedure on different data sets.
       This manual page contains only the very basic information on the synop-
       sis of Compare. The full description is maintained as a TeXinfo manual.

OOPPTTIIOONNSS
       --ff _f_i_l_e
              Sets the master setup file to _f_i_l_e.

       --pp _p_a_t_h
              Sets the master setup path to _p_a_t_h.
              If  the  path  name  is not to be completed within the following
              setup it must be given containing a trailing slash.

       --DD _p_a_t_h
              Sets the master data path to _p_a_t_h.
              If the path name is not to be  completed  within  the  following
              setup it must be given containing a trailing slash.

       --ii _t_a_g Sets the initial tag for start of setup to _t_a_g.

       --II _t_a_g Sets the initial tag for end of setup to _t_a_g.

       --aa     Create initial configuration for data files residing in the path
              specified by the --DD command

       --AA _f_i_l_e
              Sets the auto configuration setup file to _f_i_l_e.

       --TT _t_a_g Defines a tag to be used in the setup.
              The syntax is the same as in the setup files, i.e.  for 'TAG' to
              be replaced by 'VALUE' use '-T "TAG VALUE"'

       --FF _f_i_l_e
              Writes the generated histograms as a ROOT file to _f_i_l_e_._r_o_o_t

       --BB _f_i_l_e
              Generates  the  output  into  a  .ps booklet which is written to
              _f_i_l_e_._p_s.

       --HH _f_i_l_e
              Reads the TeX header for option --BB from _f_i_l_e.

       --CC _f_i_l_e
              Reads the TeX tail for option --BB from _f_i_l_e.

       --WW _f_i_l_e
              Generates the output into an .html document which is written  to
              _f_i_l_e_._h_t_m_l.

       --SS _f_i_l_e
              Reads the HTML header for option --WW from _f_i_l_e.

       --EE _f_i_l_e
              Reads the HTML tail for option --WW from _f_i_l_e.

       --ww _t_i_t_l_e
              Sets the title of the HTML document for option --WW to _t_i_t_l_e.

       --PP _p_a_t_h
              Sets the ROOT, TeX and HTML output path to _p_a_t_h.

       --tt _t_y_p_e
              Defines the output file type for histograms.  Possible types are
              _e_p_s, _g_i_f and _j_p_e_g.  If histograms are to be created as something
              but _e_p_s,

       --oo     Overwrites  old figure files generated in connection with --BB and
              --WW.

       --kk     Does not overwrite old .tex and .html files generated in connec-
              tion with --BB and --WW.

       --bb     Runs in non-batch mode, i.e. displays the results online.

       --NN     Displays the histogram locations.

       --nn     Displays the reader index.

       --OO _l_e_v_e_l
              Sets the output level.

       --hh     Prints a help message.

EEXXAAMMPPLLEE
       This is an example for the first invocation of Compare

         CCoommppaarree --ff ccoonnffiigg..ddaatt --pp ccoonnffiigg// --BB bbooookklleett
                 --PP oouuttppuutt// --DD iinnppuutt// --aa

       The  program uses a configuration specified in the base file _c_o_n_f_i_g_._d_a_t
       residing in the directory _c_o_n_f_i_g_/. It is called in batch mode, creating
       a  booklet file _b_o_o_k_l_e_t_._p_s. All output files will be put in a directory
       named _o_u_t_p_u_t_/. The histogram input data are  read  from  the  directory
       _i_n_p_u_t_/.  Due to the --aa flag, a basic configuration is created into _c_o_n_-
       _f_i_g_._d_a_t by Compare itself.

FFIILLEESS
       The default auto configurator setup file _a_u_t_o_c_o_n_f_i_g_._d_a_t can be found in
       the sshhaarree directory.

AAUUTTHHOORRSS
       Compare  was  written  by  Stefan  Hoeche, Andreas Schaelicke and Frank
       Siegert.

CCOOPPYYRRIIGGHHTT
       Compare is free software.  You can redistribute  it  and/or  modify  it
       under  the  terms of the GNU General Public License as published by the
       Free Software Foundation.  You should have received a copy of  the  GNU
       General  Public License along with the source for Compare; see the file
       COPYING.  If not, write to the  Free  Software  Foundation,  59  Temple
       Place, Suite 330, Boston, MA  02111-1307, USA.

       Compare  is distributed in the hope that it will be useful, but WITHOUT
       ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY  or
       FITNESS  FOR  A PARTICULAR PURPOSE.  See the GNU General Public License
       for more details.



Compare                           27.10.2006                        Compare(1)
