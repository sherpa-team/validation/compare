.TH Compare 1 27.10.2006 Compare ""
.SH NAME
Compare \- Program to plot histograms
.SH SYNOPSIS
Compare 
.B "[\-A \fIconfigfile\fP] [\-p \fIsetuppath\fP] [\-f \fIsetupfile\fP]"
.br
.B "        [\-D \fIdatapath\fP] [\-i \fIfilebegin\fP] [\-I \fIfileend\fP]"
.br
.B "        [\-T \fItags\fP] [\-P \fIoutpath\fP] [\-F \fIrootfile\fP]"
.br
.B "        [\-B \fItexfile\fP] [\-H \fItexheader\fP] [\-C \fItextail\fP]"
.br
.B "        [\-W \fIhtmlfile\fP] [\-S \fIhtmlheader\fP] [\-E \fIhtmltail\fP]"
.br
.B "        [\-w \fIhtmltitle\fP] [\-t \fIfiguretype\fP] [\-g \fIfiguretype\fP]"
.br
.B "        [\-O \fIoutputlevel\fP] [\-aokbneh]" 
.SH DESCRIPTION
Compare is an extension to the ROOT framework. It is dedicated to plot
large amounts of data as produced for example when analysing the output
of Monte Carlo event generators. In this context Compare can be invoked 
to plot histograms of particle spectra, eventually including data, 
generate difference plots, rebin histograms, carry out algebraic operations
on the histograms, integrate or differentiate them and write newly created 
histograms to disk. Once a full setup, specifying what to do with the input 
data is generated, input paths and files can be easily changed to carry out 
the same procedure on different data sets.
.br 
This manual page contains only the very basic 
information on the synopsis of Compare. The full description is 
maintained as a TeXinfo manual.
.SH OPTIONS
.IP "\fB-f\fP \fIfile\fP" 
Sets the master setup file to \fIfile\fP. 
.IP "\fB-p\fP \fIpath\fP"
Sets the master setup path to \fIpath\fP.
.br
If the path name is not to be completed within
the following setup it must be given containing a 
trailing slash.
.IP "\fB-D\fP \fIpath\fP"
Sets the master data path to \fIpath\fP.
.br
If the path name is not to be completed within
the following setup it must be given containing a 
trailing slash.
.IP "\fB-i\fP \fItag\fP"
Sets the initial tag for start of setup to \fItag\fP.
.IP "\fB-I\fP \fItag\fP"
Sets the initial tag for end of setup to \fItag\fP.
.IP "\fB-a\fP"
Create initial configuration for data files 
residing in the path specified by the \fB-D\fP
command
.IP "\fB-A\fP \fIfile\fP"
Sets the auto configuration setup file to \fIfile\fP.
.IP "\fB-T\fP \fItag\fP"    
Defines a tag to be used in the setup. 
.br
The syntax is the same as in the setup files, i.e. 
for 'TAG' to be replaced by 'VALUE' 
use '-T \*(lqTAG VALUE\*(rq'
.IP "\fB-F\fP \fIfile\fP"
Writes the generated histograms as a ROOT 
file to \fIfile.root\fP
.IP "\fB-B\fP \fIfile\fP"
Generates the output into a .ps booklet which is written 
to \fIfile.ps\fP.
.IP "\fB-H\fP \fIfile\fP"    
Reads the TeX header for option \fB-B\fP from \fIfile\fP.
.IP "\fB-C\fP \fIfile\fP"    
Reads the TeX tail for option \fB-B\fP from \fIfile\fP.
.IP "\fB-W\fP \fIfile\fP"
Generates the output into an .html document which is written 
to \fIfile.html\fP.
.IP "\fB-S\fP \fIfile\fP"    
Reads the HTML header for option \fB-W\fP from \fIfile\fP.
.IP "\fB-E\fP \fIfile\fP"    
Reads the HTML tail for option \fB-W\fP from \fIfile\fP.
.IP "\fB-w\fP \fItitle\fP"    
Sets the title of the HTML document for option \fB-W\fP 
to \fItitle\fP.
.IP "\fB-P\fP \fIpath\fP"
Sets the ROOT, TeX and HTML output path to \fIpath\fP.
. This path name must always be specified with a slash.
.IP "\fB-t\fP \fItype\fP"    
Defines the figure type for webpages. 
Possible types are \fIeps\fP, \fIgif\fP and \fIjpeg\fP.
.IP "\fB-g\fP \fItype\fP"    
Defines the figure type for booklets. 
Possible types are \fIeps\fP, and \fIpdf\fP.
.IP "\fB-k\fP"
Does not overwrite old .tex and .html files 
generated in connection with \fB-B\fP and \fB-W\fP.
.IP "\fB-b\fP"           
Runs in non-batch mode, i.e. displays the results online.
.IP "\fB-N\fP"           
Displays the histogram locations.
.IP "\fB-n\fP"           
Displays the reader index.
.IP "\fB-O\fP \fIlevel\fP" 
Sets the output level.
.IP "\fB-h\fP"           
Prints a help message.
.SH EXAMPLE
This is an example for the first invocation of Compare

.RS 2
.B Compare -f config.dat -p config/ -B booklet   
.br
.RS 8
.B         -P output/ -D input/ -a
.RE
.RE

The program uses a configuration specified in the base file \fIconfig.dat\fP
residing in the directory \fIconfig/\fP. It is called in batch
mode, creating a booklet file \fIbooklet.ps\fP. All output files will
be put in a directory named \fIoutput/\fP. The histogram input data 
are read from the directory \fIinput/\fP. Due to the \fB-a\fP flag,
a basic configuration is created into \fIconfig.dat\fP by Compare itself.
.SH FILES
The default auto configurator setup file \fIautoconfig.dat\fP 
can be found in the \fBshare\fP directory.
.SH AUTHORS
Compare was written by Stefan Hoeche, Andreas Schaelicke and Frank Siegert.
.SH COPYRIGHT
Compare is free software.
You can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation.
You should have received a copy of the GNU General Public License
along with the source for Compare; see the file COPYING.
If not, write to the Free Software Foundation, 59 Temple Place,
Suite 330, Boston, MA  02111-1307, USA.
.PP
Compare is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
