AC_DEFUN([COMPARE_SETUP_BUILDSYSTEM],
[
  case "$build_os:$build_cpu:$build_vendor" in
    *darwin*:*:*)
      echo "checking for architecture ... Darwin (MacOS)"
      ldflags="-dynamic -flat_namespace"
      AC_DEFINE(ARCH_DARWIN, "1", "architecture is Darwin (MacOS)") 
      AC_DEFINE(LIB_SUFFIX, ".dylib", "lib suffix") 
      AC_DEFINE(LD_PATH_NAME, "DYLD_LIBRARY_PATH", "ld path name") ;;
    *linux*:*:*)
      echo "checking for architecture ... Linux"
      ldflags="-rdynamic"
      AC_DEFINE(ARCH_LINUX, "1", "architecture is Linux") 
      AC_DEFINE(LIB_SUFFIX, ".so", "lib suffix") 
      AC_DEFINE(LD_PATH_NAME, "LD_LIBRARY_PATH", "ld path name") ;;
    *)
      echo "checking for architecture ... unknown"
      echo "hosts type $build not yet supported. assuming unix behaviour."
      echo "possible failure due to unknown compiler/linker characteristics."
      echo "please inform us about build results at stefan@freacafe.de"
      read -t 60 -p "hit any key to continue " AKEY
      ldflags="-rdynamic"
      AC_DEFINE(ARCH_UNIX, "1", "architecture is Unix") 
      AC_DEFINE(LIB_SUFFIX, ".so", "lib suffix") 
      AC_DEFINE(LD_PATH_NAME, "LD_LIBRARY_PATH", "ld path name") ;;
  esac
  AC_SUBST(ldflags)
])

AC_DEFUN([COMPARE_SETUP_VARIABLES],
[
  COMPAREFLAGS="-pedantic -Wall -Wno-long-long"
  AC_SUBST(COMPAREFLAGS)
  AC_DEFINE(USING__COLOUR, "1", "Using coloured output")
  AC_DEFINE(PACKAGE_URL, "http://compare.freacafe.de/",
    "Compare website") 
  pkgdatadir="\${datadir}/\${PACKAGE_TARNAME}";
  AC_SUBST(pkgdatadir)
  pkglibdir="\${libdir}/\${PACKAGE_TARNAME}";
  AC_SUBST(pkglibdir)
  pkgincludedir="\${includedir}/\${PACKAGE_TARNAME}";
  AC_SUBST(pkgincludedir)
  pkgbindir="\${bindir}/\${PACKAGE_TARNAME}";
  AC_SUBST(pkgbindir)
])

AC_DEFUN([COMPARE_SETUP_CONFIGURE_OPTIONS],
[
  AC_ARG_ENABLE(svninclude,
    AC_HELP_STRING([--disable-svninclude],
      [Do not distribute SVN information.]),
    [ AC_MSG_CHECKING(whether to enable SVN synchronization)
      case "${enableval}" in
        no) AC_MSG_RESULT(no); SVNINCLUDE="";;
        yes) AC_MSG_RESULT(yes); SVNINCLUDE=".svn";;
      esac ],
    [ AC_MSG_CHECKING(whether to enable SVN synchronization); 
      AC_MSG_RESULT(yes); SVNINCLUDE=".svn" ] 
  )
  AC_SUBST(SVNINCLUDE)
])

AC_DEFUN([ROOT_PATH],
[
  AC_ARG_WITH(rootsys,
  [  --with-rootsys          top of the ROOT installation directory],
    user_rootsys=$withval,
    user_rootsys="none")
  if test ! x"$user_rootsys" = xnone; then
    rootbin="$user_rootsys/bin"
  elif test ! x"$ROOTSYS" = x ; then 
    rootbin="$ROOTSYS/bin"
  else 
   rootbin=$PATH
  fi
  AC_PATH_PROG(ROOTCONF, root-config , no, $rootbin)
  AC_PATH_PROG(ROOTEXEC, root , no, $rootbin)
  AC_PATH_PROG(ROOTCINT, rootcint , no, $rootbin)
  if test ! x"$ROOTCONF" = "xno" && \
     test ! x"$ROOTCINT" = "xno" ; then 
    ROOTLIBDIR=`$ROOTCONF --libdir`
    ROOTINCDIR=`$ROOTCONF --incdir`
    ROOTCFLAGS=`$ROOTCONF --noauxcflags --cflags` 
    ROOTLIBS=`$ROOTCONF --noauxlibs --noldflags --libs`
    ROOTGLIBS=`$ROOTCONF --noauxlibs --noldflags --glibs`
    ROOTAUXCFLAGS=`$ROOTCONF --auxcflags`
    ROOTAUXLIBS=`$ROOTCONF --auxlibs`
    ROOTRPATH=$ROOTLIBDIR
    if test $1 ; then 
      AC_MSG_CHECKING(whether ROOT version >= [$1])
      vers=`$ROOTCONF --version | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      requ=`echo $1 | tr './' ' ' | awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      if test $vers -lt $requ ; then 
        AC_MSG_RESULT(no)
	no_root="yes"
      else 
        AC_MSG_RESULT(yes)
      fi
    fi
  else
    no_root="yes"
  fi
  AC_SUBST(ROOTLIBDIR)
  AC_SUBST(ROOTINCDIR)
  AC_SUBST(ROOTCFLAGS)
  AC_SUBST(ROOTLIBS)
  AC_SUBST(ROOTGLIBS) 
  AC_SUBST(ROOTAUXLIBS)
  AC_SUBST(ROOTAUXCFLAGS)
  AC_SUBST(ROOTRPATH)
  if test "x$no_root" = "x" ; then 
    ifelse([$2], , :, [$2])     
  else 
    ifelse([$3], , :, [$3])     
  fi
])
