#include "Histogram_Reader_Base.H"
#include "Message.H"
#include "MyStrStream.H"

#include "MathTools.H"
#include "MyStrStream.H"
#include "Compare_Histogram.H"
#include "My_Limits.H"

#include <iomanip>
#include <algorithm>

using namespace COMPARE;
using namespace ATOOLS;

#define COMPILE__Getter_Function
#define EXACTMATCH false
#define OBJECT_TYPE Histogram_Reader_Base
#define PARAMETER_TYPE Histogram_Handler_Base *const
#include "Getter_Function.C"

Histogram_Reader_Base::Reader_Map Histogram_Reader_Base::s_readermap;
Histogram_Reader_Base::Reader_Map Histogram_Reader_Base::s_aliasmap;

Histogram_Reader_Base::
Histogram_Reader_Base(Histogram_Handler_Base *_p_handler):
  p_handler(_p_handler),
  p_histogram(NULL),
  p_info(_p_handler->Info()),
  p_alldata(NULL),
  p_data(new std::vector<std::vector<double> >()),
  m_entries(0.0)
{
}

Histogram_Reader_Base::~Histogram_Reader_Base()
{
  delete p_data;
  if (p_alldata!=NULL)    delete p_alldata;
}

bool Histogram_Reader_Base::CreateBins(ATOOLS::Axis<double> *axis) 
{
  if (p_data->size()<2 || (*p_data)[0].size()<2) return false; 
  m_bins.clear();
  size_t i;
  Double_t x, nx;
  Double_t xmin((*p_info)(dic::cutoff_x_min)?(*p_info)[dic::cutoff_x_min]:
		-std::numeric_limits<double>::max());
  Double_t xmax((*p_info)(dic::cutoff_x_max)?(*p_info)[dic::cutoff_x_max]:
		std::numeric_limits<double>::max());
  for (i=0;i<(*p_data)[0].size();++i) {
    if (p_data->size()>3 && 
	((*p_data)[2][i]!=0.0 || (*p_data)[3][i]!=0.0)) {
      x=(*axis)((*p_data)[0][i]-(*p_data)[2][i]);
      nx=(*axis)((*p_data)[0][i]+(*p_data)[3][i]);
    }
    else {
      x=(*axis)((*p_data)[0][i]);
      if (i+1<(*p_data)[0].size()) nx=(*axis)((*p_data)[0][i+1]);
      else nx=2.0*(*axis)((*p_data)[0][i])-(*axis)((*p_data)[0][i-1]);
    }
    if (nx>=xmin && x<=xmax) m_bins.push_back(x);
  }
  if (p_data->size()>3 && 
      ((*p_data)[2][i-1]!=0.0 || (*p_data)[3][i-1]!=0.0)) 
    x=(*axis)((*p_data)[0][i-1]+(*p_data)[3][i-1]);
  else
    x=2.0*(*axis)((*p_data)[0][i-1])-(*axis)((*p_data)[0][i-2]);
  if (x>=xmin && x<=xmax) m_bins.push_back(x);
  return true;
}

bool Histogram_Reader_Base::RescaleData(Scale_Function *const xscale,
					Scale_Function *const xerrscale,
					Scale_Function *const yscale,
					Scale_Function *const yerrscale) 
{
  if (p_data->size()==0) return false;
  Data_Reader &reader(p_handler->DataReader());
  reader.SetString(reader.StripEscapes((*p_info)[sic::reader_parameters]));
  std::string protect;
  if (reader.ReadFromString(protect,"DISALLOW_RESCALE")) {
    if (protect.find("YES")!=std::string::npos) return true;
  }
  for (size_t i=0;i<(*p_data)[1].size();++i) {
    double x((*p_data)[0][i]), y((*p_data)[1][i]);
    (*p_data)[0][i]=(*xscale)(x,y);
    (*p_data)[1][i]=(*yscale)(x,y);
    for (size_t j(2);j<Min((size_t)4,p_data->size());++j)
      (*p_data)[j][i]=(*xerrscale)(x,y,(*p_data)[j][i],0.0);
    for (size_t j(4);j<p_data->size();++j)
      (*p_data)[j][i]=(*yerrscale)(x,y,0.0,(*p_data)[j][i]);
  }
  if (p_alldata!=NULL && p_alldata!=p_data) {
    if (reader.ReadFromString(protect,"RESCALE_DATA"))
      if (protect.find("YES")!=std::string::npos) {
	for (size_t i=0;i<(*p_alldata)[1].size();++i) {
	  double x((*p_alldata)[0][i]), y((*p_alldata)[1][i]);
	  (*p_alldata)[0][i]=(*xscale)(x,y);
	  (*p_alldata)[1][i]=(*yscale)(x,y);
	  for (size_t j(2);j<Min((size_t)4,p_alldata->size());++j)
	    (*p_alldata)[j][i]=(*xerrscale)(x,y,(*p_alldata)[j][i],0.0);
	  for (size_t j(4);j<p_alldata->size();++j)
	    (*p_alldata)[j][i]=(*yerrscale)(x,y,0.0,(*p_alldata)[j][i]);
	}
      }
  }
  Compare_Histogram<double> histo;
  histo.Import(*p_data);
  p_handler->SetLocalTag("RESCALED_HISTOGRAM_NORM",ToString(histo.Norm()));
  p_handler->SetLocalTag("RESCALED_HISTOGRAM_MEAN",ToString(histo.Mean()));
  return true;
}

bool Histogram_Reader_Base::DecentralizeBins(ATOOLS::Axis<double> *axis) 
{
  if (p_alldata!=NULL) {
  if (p_alldata->size()<2 || (*p_alldata)[0].size()<2) {
    msg_Info()<<METHOD<<"("<<axis<<"): No data. Abort."<<std::endl;
    return false;
  }
  if ((*p_handler->Info())[iic::central_x]!=0) {
    std::vector<double> newx((*p_data)[0].size());
    if ((*p_handler->Info())[iic::central_x]>0) {
      newx[0]=(*axis)[(3.0*(*axis)((*p_data)[0][0])-
		       (*axis)((*p_data)[0][1]))/2.0];
      for (size_t i=1;i<(*p_data)[0].size();++i) {
	newx[i]=(*axis)[((*axis)((*p_data)[0][i])+
			 (*axis)((*p_data)[0][i-1]))/2.0];
      }
    }
    else {
      for (size_t i=1;i<(*p_data)[0].size();++i) {
	newx[i-1]=(*axis)[((*axis)((*p_data)[0][i])+
			   (*axis)((*p_data)[0][i-1]))/2.0];
      }
      newx.back()=
	(*axis)[(3.0*(*axis)((*p_data)[0].back())-
		 (*axis)((*p_data)[0][p_data->size()-2]))/2.0];
    }
    (*p_data)[0]=newx;
  }
  }
  Compare_Histogram<double> histo;
  histo.Import(*p_data);
  p_handler->SetLocalTag("HISTOGRAM_NORM",ToString(histo.Norm()));
  p_handler->SetLocalTag("HISTOGRAM_MEAN",ToString(histo.Mean()));
  msg_Debugging()<<METHOD<<"(): norm = "<<histo.Norm()
		 <<", mean = "<<histo.Mean()<<"\n";
  return true;
}

bool Histogram_Reader_Base::ResizeBins(ATOOLS::Axis<double> *axis) 
{
  if (p_data->size()==0) return false;
  std::vector<std::vector<double> > resize;
  Data_Reader &reader(p_handler->DataReader());
  reader.SetString(reader.StripEscapes((*p_info)[sic::reader_parameters]));
  std::string protect;
  if (reader.ReadFromString(protect,"DISALLOW_RESIZE")&&
      protect.find("YES")!=std::string::npos) return true;
  bool data=true;
  if (reader.ReadFromString(protect,"REBIN_DATA")&&
      protect.find("NO")!=std::string::npos) data=false;
  String_Vector resizetag;
  reader.VectorFromString(resizetag,"RESIZE_BINS");
  reader.SetLineSeparator(",");
  reader.SetString(p_handler->MakeString(resizetag));
  reader.MatrixFromString(resize);
  reader.SetLineSeparator(";");
  if (resize.size()<1) return true;
  msg_Info()<<METHOD<<"(..): data="<<data<<" {\n";
  size_t i=0;
  for (;i<resize.size();++i) {
    if (resize[i].size()!=2) {
      std::vector<std::vector<double> >::iterator pos;
      for (pos=resize.begin();pos!=resize.end();++pos) {
	if (*pos==resize[i]) {
	  resize.erase(pos);
	  break;
	}
      }
      --i;
      if (resize.size()==0) resize.push_back(std::vector<double>(2,1));
      continue;
    }
    resize[i][0]=Max(resize[i][0],(*axis)((*p_data)[0][0]));
    if (i>0) {
      msg_Info()<<"   combining ("<<std::setw(2)<<resize[i-1][1]
		<<") bins in [ "<<std::setw(8)<<resize[i-1][0]<<" , "
		<<std::setw(8)<<resize[i][0]<<" )"<<std::endl;
    }
  }
  msg_Info()<<"   combining ("<<std::setw(2)<<resize[i-1][1]
	    <<") bins in [ "<<std::setw(8)<<resize[i-1][0]<<" , "
	    <<std::setw(8)<<(*axis)((*p_data)[0][(*p_data)[0].size()-1])
	    <<" ]"<<std::endl;
  msg_Info()<<"}"<<std::endl;
  for (size_t i=0,j=0;j<(*p_data)[0].size();++j) {
    if ((*p_data)[0][j]<resize[0][0]) continue;
    if (i<resize.size()-1 && (*p_data)[0][j]>resize[i+1][0]) ++i;
    int di=0, usez=p_info->Info(iic::z_value_column,di);
    double xm=(*p_data)[0][j]-(*p_data)[2][j];
    double xp=(*p_data)[0][j]+(*p_data)[3][j];
    double x=0.0, y=0.0, y2m=0.0, y2p=0.0, z=0.0, z2m=0.0, z2p=0.0;
    size_t k=0;
    for (;k<resize[i][1] && j<(*p_data)[0].size();++k) {
      double cz=usez?(*p_data)[6][j]:1.0, cy=(*p_data)[1][j]*cz;
      z+=cz;
      y+=cy;
      x+=(*p_data)[0][j];
      if (!usez) {
	if (p_data->size()>4) {
	  y2m+=(m_entries-1.0)*sqr((*p_data)[4][j])+sqr((*p_data)[1][j]);
	  y2p+=(m_entries-1.0)*sqr((*p_data)[5][j])+sqr((*p_data)[1][j]);
	}
      }
      else {
	double dam=cy*((*p_data)[4][j]/(*p_data)[1][j]-(*p_data)[7][j]/cz);
	double dap=cy*((*p_data)[5][j]/(*p_data)[1][j]-(*p_data)[8][j]/cz);
	y2m+=cz?(m_entries-1.0)*sqr(dam)+sqr(cy):0.0;
	y2p+=cz?(m_entries-1.0)*sqr(dap)+sqr(cy):0.0;
	z2m+=(m_entries-1.0)*sqr((*p_data)[7][j])+sqr((*p_data)[1][j]);
	z2p+=(m_entries-1.0)*sqr((*p_data)[8][j])+sqr((*p_data)[1][j]);
      }
      xp=(*p_data)[0][j]+(*p_data)[3][j];
      if (k<resize[i][1]-1) {
	std::vector<double>::iterator pos;
	for (short unsigned int l=0,m=0;l<p_data->size();++l) {
	  for (m=0,pos=(*p_data)[l].begin();pos!=(*p_data)[l].end();++pos) {
	    if (++m==j+1) {
	      if (m<(*p_data)[l].size()-1) (*p_data)[l].erase(pos);
	      break;
	    }
	  }
	}
      }
    }
    (*p_data)[0][j]=x/k;
    (*p_data)[1][j]=y/z;
    (*p_data)[2][j]=(*p_data)[0][j]-xm;
    (*p_data)[3][j]=xp-(*p_data)[0][j];
    if (!usez) {
      if (p_data->size()>4) {
	(*p_data)[4][j]=sqrt((y2m/(z*z)-sqr((*p_data)[1][j]))/(m_entries-1.0));
	(*p_data)[5][j]=sqrt((y2p/(z*z)-sqr((*p_data)[1][j]))/(m_entries-1.0));
      }
    }
    else {
      (*p_data)[6][j]=z;
      (*p_data)[7][j]=sqrt((z2m-z*z)/(m_entries-1.0));
      (*p_data)[8][j]=sqrt((z2p-z*z)/(m_entries-1.0));
      double sym=sqrt((y2m-sqr(y))/(m_entries-1.0));
      double syp=sqrt((y2p-sqr(y))/(m_entries-1.0));
      (*p_data)[4][j]=(*p_data)[1][j]*(sym/y+(*p_data)[7][j]/z);
      (*p_data)[5][j]=(*p_data)[1][j]*(syp/y+(*p_data)[8][j]/z);
    }
  }
  if (p_alldata==NULL || !data) return true;
  for (size_t i=0,j=0;j<(*p_alldata)[0].size();++j) {
    if ((*p_alldata)[0][j]<resize[0][0]) continue;
    if (i<resize.size()-1 && (*p_alldata)[0][j]>resize[i+1][0]) ++i;
    double x=(*p_alldata)[0][j];
    std::vector<double> ys(p_alldata->size(),0.0);
    size_t k=0;
    for (;k<resize[i][1] && j<(*p_alldata)[0].size();++k) {
      for (size_t n=1; n<p_alldata->size();++n) ys[n]+=(*p_alldata)[n][j];
      if (k<resize[i][1]-1) {
	std::vector<double>::iterator pos;
	for (short unsigned int l=0,m=0;l<p_alldata->size();++l) {
	  for (m=0,pos=(*p_alldata)[l].begin();
	       pos!=(*p_alldata)[l].end();++pos) {
	    if (++m==j+1) {
	      if (m<(*p_alldata)[l].size()-1) (*p_alldata)[l].erase(pos);
	      break;
	    }
	  }
	}
      }
    }
    (*p_alldata)[0][j]=x;
    for (size_t n=1; n<p_alldata->size();++n) 
      (*p_alldata)[n][j]=ys[n]/(double)k;
  }
  return true;
}

bool Histogram_Reader_Base::AdoptBins() 
{
  if (p_data->size()==0) return false;
  Data_Reader &reader(p_handler->DataReader());
  reader.SetString(reader.StripEscapes((*p_info)[sic::reader_parameters]));
  std::string protect;
  if (reader.ReadFromString(protect,"DISALLOW_ADOPT")) {
    if (protect.find("YES")!=std::string::npos) return true;
  }
  bool data=false;
  if (reader.ReadFromString(protect,"ADOPT_DATA")) {
    if (protect.find("YES")!=std::string::npos) data=true;
  }
  std::string mastername, first, last;
  if (!reader.ReadFromString(mastername,"ADOPT_BINS")) return true;
  reader.ReadFromString(last,"LAST_BIN_UPPER");
  reader.ReadFromString(first,"FIRST_BIN_LOWER");
  Histogram_Reader_Base *master=NULL;
  if (s_readermap.find(mastername)==s_readermap.end()) {
    if (s_aliasmap.find(mastername)==s_aliasmap.end()) {
      msg_Error()<<METHOD<<"(..): Cannot find master reader '"
		 <<mastername<<"'."<<std::endl;
      return false;
    }
    else {
      master=s_aliasmap[mastername];
    }
  }
  else {
    master=s_readermap[mastername];
  }
  std::vector<std::vector<double> > *const adopt=master->Data();
  if (adopt->size()<1) return false;
  msg_Info()<<"Histogram_Reader_Base::AdoptBins(..): Master is '"
	    <<mastername<<"'"<<std::endl;
  double center=0., left=0., right=0., l=0., r=0.;
  double leftvalue=0., rightvalue=0.;
  int shift=-1;
  std::vector<std::vector<double> > *newdata;
  newdata = new std::vector<std::vector<double> >
    (adopt->size(),std::vector<double>((*adopt)[0].size(),0.0));
  for (size_t i=0;i<=(*adopt)[0].size();++i) {
    bool forceleft=false, forceright=false;
    if (i==0) {
      if (adopt->size()>3 && 
	  ((*adopt)[2][i]!=0.0 || (*adopt)[3][i]!=0.0)) {
	center=(*adopt)[0][i];
	left=center+(*adopt)[3][i]-2.0*(*adopt)[2][i];
	right=center-(*adopt)[2][i];
      }
      else {
	left=2.0*(*adopt)[0][i+1]-(*adopt)[0][i];
	right=(*adopt)[0][i];
	center=(left+right)/2.0;
      }
      if (first.find("BEGIN")!=std::string::npos) {
	left=(*p_data)[0][0];
	if (left>right) continue;
      }
      else if (first.length()>0) { 
	leftvalue=ToType<double>(first);
	if (leftvalue>right) continue;
	forceleft=true;
      }
      else {
	continue;
      }
      for (size_t d(0);d<newdata->size();++d)
	(*newdata)[d].push_back(0.);
      ++shift;
    }
    else if (i==(*adopt)[0].size()) {
      if (adopt->size()>3 && 
	  ((*adopt)[2][i-1]!=0.0 || (*adopt)[3][i-1]!=0.0)) {
	center=(*adopt)[0][i-1];
	left=center-(*adopt)[2][i-1];
	right=center+(*adopt)[3][i-1];
      }
      else {
	left=(*adopt)[0][i-1];
	right=2.0*(*adopt)[0][i-1]-(*adopt)[0][i-2];
	center=(left+right)/2.0;
      }
      if (last.find("END")!=std::string::npos) {
	right=(*p_data)[0][(*p_data)[0].size()-1];
	if (right<left) break;
      }
      else if (last.length()>0) { 
	rightvalue=ToType<double>(last);
	if (rightvalue<left) break;
	forceright=true;
      }
      for (size_t d(0);d<newdata->size();++d)
	(*newdata)[d].push_back(0.);
    }
    else {
      if (adopt->size()>3 && 
	  ((*adopt)[2][i-1]!=0.0 || (*adopt)[3][i-1]!=0.0)) {
	center=(*adopt)[0][i-1];
	left=center-(*adopt)[2][i-1];
	right=center+(*adopt)[3][i-1];
      }
      else {
	left=(*adopt)[0][i-1];
	right=(*adopt)[0][i];
	center=(left+right)/2.0;
      }
    }
    if (forceleft) left=leftvalue;
    if (forceright) right=rightvalue;
    (*newdata)[0][i+shift]=left;
    (*newdata)[1][i+shift]=0.;
    if (newdata->size()>3) {
      (*newdata)[0][i+shift]=center;
      (*newdata)[2][i+shift]=center-left;
      (*newdata)[3][i+shift]=right-center;
    }
    double binwidth=0.;
    for (size_t j=0;j<(*p_data)[0].size();++j) {
      if (p_data->size()>3 && 
	  ((*p_data)[2][j]!=0.0 || (*p_data)[3][j]!=0.0)) {
	l=Max(left,(*p_data)[0][j]-(*p_data)[2][j]);
	r=Min(right,(*p_data)[0][j]+(*p_data)[3][j]);
      }
      else {
	l=Max(left,(*p_data)[0][j]);
	r=Min(right,j<(*p_data)[0].size()-1?(*p_data)[0][j+1]:
	      2.0*(*p_data)[0][j]-(*p_data)[0][j-1]);
      }
      if (l<r) {
	binwidth+=r-l;
	(*newdata)[1][i+shift]+=(*p_data)[1][j]*(r-l);
      }
    }
    if (binwidth>0.0) (*newdata)[1][i+shift]/=binwidth;
    msg_Tracking()<<"   bin ("<<std::setw(3)<<i+shift<<"): [ "
		  <<std::setw(8)<<left<<" , "
		  <<std::setw(8)<<center<<" , "
		  <<std::setw(8)<<right<<" ] = "<<std::setw(12)
		  <<(*newdata)[1][i+shift]<<std::endl;
  }
  (*newdata)[0][(*newdata)[0].size()-1]=right;
  (*newdata)[1][(*newdata)[0].size()-1]=0.;
  if (newdata->size()>3) {
    for (size_t d(0);d<newdata->size();++d)
      (*newdata)[d].pop_back();    
  }
  *p_data=*newdata;
  if (p_alldata!=NULL && data) *p_alldata=*newdata;
  delete newdata;
  return true;
}

bool Histogram_Reader_Base::ReadData() 
{
  msg_Error()<<"Histogram_Reader_Base::ReadData(): "
	     <<"Virtual Function called !"<<std::endl;
  return false;
}

bool Histogram_Reader_Base::WriteData() 
{
  return true;
}

class Order_X {
public:
  bool operator()(const std::vector<double> &a,
		  const std::vector<double> &b)
  { return a.front()<b.front(); }
};// end of class Order_X

bool Histogram_Reader_Base::SortData(DataType &data)
{
  if (data.empty() || data.front().empty()) return true;
  DataType trans(data.front().size(),std::vector<double>(data.size()));
  for (size_t i(0);i<data.front().size();++i)
    for (size_t j(0);j<data.size();++j)
      trans[i][j]=data[j][i];
  std::sort(trans.begin(),trans.end(),Order_X());
  for (size_t i(0);i<data.front().size();++i)
    for (size_t j(0);j<data.size();++j)
      data[j][i]=trans[i][j];
  return true;
}

bool Histogram_Reader_Base::SortData()
{
  if (p_alldata!=NULL && !SortData(*p_alldata)) return false; 
  if (p_data!=NULL && !SortData(*p_data)) return false; 
  return true;
}

int Histogram_Reader_Base::SetAliasName() 
{
  std::string helps, name;
  if (!p_info->Info(sic::alias_name,helps)) return -1;
  int i=-1;
  while (true) { 
    name=helps;
    name+=std::string("[")+ToString(++i)+std::string("]"); 
    if (s_aliasmap.find(name)==s_aliasmap.end()) break; 
  } 
  if (i==0) s_aliasmap[helps]=this;
  s_aliasmap[name]=this;
  msg_Debugging()<<METHOD<<"(): Set alias name '"<<name<<"'.\n";
  return i;
}

void Histogram_Reader_Base::PrintHistogramNames() 
{
  if (s_readermap.empty()) return;
  msg_Out()<<om::bold<<METHOD<<"(): {\n"<<om::reset;
  for (Reader_Map::const_iterator ait=s_readermap.begin();
       ait!=s_readermap.end();++ait)
    if (ait->second->p_data && 
	ait->second->p_data->size() && 
	ait->second->p_data->front().size() &&
	ait->first.find("(0x")!=0) {
      msg_Out()<<"   ("<<ait->second<<") -> \""
               <<om::red<<ait->first<<om::reset<<"\"\n";
      /*
      std::string f=ait->first;
      std::string s=
        std::string("export LANGUAGE=C; ")+
        std::string("export LC_ALL=C; ")+
        std::string("export LANG=C; ")+
        std::string("for i in $(seq 1 9); do ")+
        std::string("    part=$(echo \""+f+"\" | cut -d \"/\" -f \"1-\"$i); ")+
        std::string("    if test -d $part; then ")+
        std::string("        if test -d $part/.svn; then continue; ")+
        std::string("        else ")+
        std::string("            svn add --non-recursive \"$part\"; ")+
        std::string("        fi; ")+
        std::string("    fi; ")+
        std::string("done; ")+
        std::string("svn add \""+f+"\"; ");
      system(("bash -c '"+s+"'").c_str());
      */
    }
  msg_Out()<<om::bold<<"}"<<om::reset<<std::endl;
}

void Histogram_Reader_Base::PrintAliasTable() 
{
  if (s_aliasmap.size()==0) return;
  msg_Info()<<om::bold<<"Histogram_Reader_Base::PrintAliasTable(): "
	    <<"Aliases are {\n"<<om::reset;
  for (Reader_Map::const_iterator ait=s_aliasmap.begin();
       ait!=s_aliasmap.end();++ait) {
    msg_Info()<<"   \"";
    if (ait->first.find("]")!=
	ait->first.length()-1) msg_Info()<<om::bold;
    msg_Info()<<om::blue<<ait->first<<om::reset<<"\" ->";
    bool found=false;
    for (Reader_Map::const_iterator rit=s_readermap.begin();
	 rit!=s_readermap.end();++rit) {
      if (rit->second==ait->second && 
	  rit->first.find(ait->first)!=0 && 
	  ait->first.find(rit->first)!=0) {
	msg_Info()<<" \""<<om::red<<rit->first
		  <<om::reset<<"\"\n";
	found=true;
      }
    }
    if (!found) msg_Info()<<om::bold<<"<error: no entry>"
			  <<om::reset<<"\"\n";
  }
  msg_Info()<<om::bold<<"}"<<om::reset<<std::endl;
}

const  std::vector<std::string> & Histogram_Reader_Base::CompleteFiles()
{
  m_completefiles.clear();
  size_t i=0;
  for (;i<p_handler->Files().size();++i) {
    if (i<p_handler->Paths().size()) 
      m_completefiles.push_back(p_handler->Paths()[i]+
				p_handler->Files()[i]);
    else m_completefiles.push_back(p_handler->Paths().back()+
				   p_handler->Files()[i]);
  }
  for (;i<p_handler->Paths().size();++i) {
     m_completefiles.push_back(p_handler->Paths()[i]+
			       p_handler->Files().back());
  }
  return m_completefiles;
}

bool Histogram_Reader_Base::
Apart(const std::string &input,std::string &operation,
      std::string &leftpiece,std::string &rightpiece,
      std::string &leftrest,std::string &rightrest)
{
  int open=0, openpos=-1, closepos=-1;
  int beginoperatorpos=-1, endoperatorpos=-1;
  for (size_t i=0;i<input.length();++i) {
    if (input[i]=='(') ++open; 
    else if (input[i]==')' && open>0) --open; 
    if (open==0) {
      if (input[i]==')' && endoperatorpos>0) {
	closepos=i;
	break;
      }
    }
    else if (open==1) {
      if (input[i]=='(' && openpos==-1) openpos=i;
      else if (input[i]==')' && beginoperatorpos!=-1 && 
	       endoperatorpos==-1) endoperatorpos=i;
    }
    else if (open==2) {
      if (input[i]=='(' && input.length()>i+2 && 
	  beginoperatorpos==-1) {
 	if (input[i+2]=='+' || input[i+2]=='-' || 
 	    input[i+2]=='*' || input[i+2]=='/' || 
 	    input[i+2]=='I' || input[i+2]=='D') {
	  beginoperatorpos=i;
	}
	else {
	  --open;
	  openpos=i;
	}
      }
    }
  }
  if (openpos==-1 || beginoperatorpos==-1 || 
      endoperatorpos==-1 || closepos==-1) return false;
  leftpiece=input.substr(openpos+1,beginoperatorpos-openpos-1);
  rightpiece=input.substr(endoperatorpos+1,closepos-endoperatorpos-1);
  leftrest=input.substr(0,openpos);
  rightrest=input.substr(closepos+1);
  operation=input.substr(beginoperatorpos,
			 endoperatorpos-beginoperatorpos+1);
  return true;
}

void Histogram_Reader_Base::
GenerateName(ATOOLS::Node<std::string> *node,size_t catcher)
{
  for (size_t i=0;i<3;++i) (*node).push_back("");
  if (node->operator->()==NULL || ++catcher>100) return;
  std::string left, right, operation, leftrest, rightrest;
  if (Apart((*node)[0],operation,left,right,leftrest,rightrest)) {
    (*node)[1]=operation;
    (*node)[2]=leftrest;
    (*node)[3]=rightrest;
  }
  bool create=operation.size()>0;
  Node<std::string> *leftnode = new 
    Node<std::string>(left,create);
  Node<std::string> *rightnode = new 
    Node<std::string>(right,create);
  GenerateName(leftnode,catcher);
  GenerateName(rightnode,catcher);
  (*node)->push_back(leftnode);
  (*node)->push_back(rightnode);
}

bool Histogram_Reader_Base::
ExtractName(std::vector<std::string> &names,
	    const std::string &leftname,const std::string &rightname,
	    ATOOLS::Node<std::string> *node,size_t catcher)
{
  if (node->operator->()==NULL || ++catcher>100) {
    return true;
  }
  if (ExtractName(names,leftname+(*node)[2],
		  (*node)[3]+rightname,(*node)->operator[](0),catcher) ||
      ExtractName(names,leftname+(*node)[2],
		  (*node)[3]+rightname,(*node)->operator[](1),catcher)) {
    names.push_back(leftname+(*node)[2]+(*node)[0]+(*node)[3]+rightname);
  }
  else {
    if ((*node)[1]!=std::string("")) {
      names[names.size()-2]="("+names[names.size()-2]+
	(*node)[1]+names[names.size()-1]+")";
    }
    names.erase(names.end());
  }
  return false;
}

std::vector<std::string> Histogram_Reader_Base::
GenerateNames(const std::string &name)
{
  std::vector<std::string> names;
  Node<std::string> *master = new Node<std::string>(name,true);
  GenerateName(master,0);
  ExtractName(names,"","",master,0);
  delete master;
  return names;
}

Histogram_Reader_Base *const
Histogram_Reader_Base::GetReader(Histogram_Handler_Base *handler,
				 const std::string &key,const bool create) 
{
  std::string alias=GenerateNames(key).back();
  if (handler!=NULL) alias+=handler->GetReaderKey();
  Reader_Map::iterator rit=s_readermap.find(alias);
  if (rit!=s_readermap.end()) return rit->second;
  for (Reader_Map::reverse_iterator ait=s_aliasmap.rbegin();
       ait!=s_aliasmap.rend();++ait)
    if (alias.find(ait->first)!=std::string::npos) return ait->second;
  if (!create) return NULL;
  return Getter_Function::GetObject(key,handler);
}

