#ifndef Shell_Tools_H
#define Shell_Tools_H

#include <string>
#include <vector>
#include <sys/stat.h>

namespace ATOOLS {

  bool MakeDir(std::string path,const mode_t mode,const bool create_tree=1);

  std::vector<std::string> EnvironmentVariable(const std::string &name,
					       std::string entry="");

  bool FileExists(const std::string &file);

}// end of namespace ATOOLS

#endif
