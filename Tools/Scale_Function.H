#ifndef Scale_Function_H
#define Scale_Function_H

#include "Algebra_Interpreter.H"
#include "Axis.H"

namespace COMPARE {

  class Scale_Function: public ATOOLS::Tag_Replacer {
  private:
    
    ATOOLS::Algebra_Interpreter m_interpreter;

    double m_xvalue, m_yvalue, m_xerror, m_yerror;

  public:

    // constructor
    Scale_Function(const std::string &function,
		   const ATOOLS::Algebra_Interpreter::String_Map &tags=
		   ATOOLS::Algebra_Interpreter::String_Map());
    // destructor
    ~Scale_Function();

    // member functions
    std::string   ReplaceTags(std::string &expr) const;    
    ATOOLS::Term *ReplaceTags(ATOOLS::Term *term) const;    

    // inline functions
    double operator()(const double &xvalue,const double &yvalue,
		      const double &xerror=0.0,const double &yerror=0.0);

  };// end of class Scale_Function

}// end of namespace COMPARE

#endif
