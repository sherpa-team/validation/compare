#include "Writeout_Reader.H"

#include "Data_Writer.H" 
#include "Message.H" 

using namespace COMPARE;
using namespace ATOOLS;

DECLARE_GETTER(Writeout_Getter,"WRITE",
	       Histogram_Reader_Base,Histogram_Handler_Base *const);

Histogram_Reader_Base *Writeout_Getter::operator()
  (Histogram_Handler_Base *const &handler) const
{ 
  return new Writeout_Reader(handler);
}

void Writeout_Getter::PrintInfo(std::ostream &str,const size_t width) const
{ 
  str<<"write out ascii"; 
}

Writeout_Reader::Writeout_Reader(Histogram_Handler_Base *handler):
  Histogram_Reader_Base(handler),
  m_outfile((*handler->Info())[sic::data_type])
{
  size_t bpos(m_outfile.find("("));
  if (bpos==std::string::npos) return;
  m_outfile=m_outfile.substr(bpos);
  if ((bpos=m_outfile.rfind(")"))==std::string::npos) return;
  m_outfile=m_outfile.substr(1,bpos-1);
  if (m_outfile.length()==0) m_outfile=Handler()->Files()[0];
}

Writeout_Reader::~Writeout_Reader() {}

bool Writeout_Reader::ReadData() 
{
  std::string path=Handler()->Paths()[0], file=Handler()->Files()[0];
  msg_Info()<<METHOD<<"(): Looking for data from '"<<path+file<<"' ... ";
  Histogram_Reader_Base *reader=GetReader(NULL,path+file);
  if (reader!=NULL) {
    msg_Info()<<"found."<<std::endl;
    std::vector<std::vector<double> > *help(reader->Data());
    if (help==NULL) {
      msg_Info()<<METHOD<<"(): Data corrupted. Abort."<<std::endl;
      return false;
    }
    if (m_outfile.length()==0) {
      msg_Info()<<METHOD<<"(): No filename given. Abort."<<std::endl;
      return false;
    }
    msg_Info()<<METHOD<<"(): Writing data to '"<<Handler()->Paths()[0]<<"/"
	      <<m_outfile<<"'"<<std::endl;
    Data_Writer writer(" ","#","#");
    writer.SetMatrixType(mtc::normal);
    writer.SetOutputPath(Handler()->Paths()[0]);
    writer.SetOutputFile(m_outfile);
    writer.MatrixToFile(*help);
    return false;
  }
  msg_Info()<<"not found."<<std::endl;
  return false;
}

